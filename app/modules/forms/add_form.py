from wtforms import Form
from wtforms import TextField
from wtforms import DecimalField
from wtforms import validators

class AddForm(Form):
    item_name = TextField('Item Name', [validators.Length(min=3, max=40),validators.regexp('^[a-zA-Z0-9\s]+$', message='Item Name Must be Alphanumeric Only!')])
    price = TextField('Price', [validators.regexp('^\d+\.\d\d$', message='Price Must have 2 decimal places (e.g. 55.00)')])
